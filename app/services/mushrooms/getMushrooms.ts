export type ColorType = "RED" | "GREEN" | "YELLOW" | "BLUE";

export type SpotsType =
  | "none"
  | "hidden"
  | "dotted"
  | "dashed"
  | "solid"
  | "double"
  | "groove"
  | "ridge"
  | "inset"
  | "outset";

export interface MushroomType {
  name: string;
  spots: SpotsType;
  color: ColorType;
  latlng: [number, number];
}

const mushrooms: MushroomType[] = [
  {
    name: "nervous bell",
    spots: "dashed",
    color: "RED",
    latlng: [52.082042, 5.236192],
  },
  {
    name: "nice benz",
    spots: "dotted",
    color: "BLUE",
    latlng: [52.080678, 5.236457],
  },
  {
    name: "quizzical chaplygin",
    spots: "double",
    color: "RED",
    latlng: [52.081624, 5.235895],
  },
  {
    name: "nifty bhabha",
    spots: "groove",
    color: "BLUE",
    latlng: [52.080671, 5.236392],
  },
  {
    name: "peaceful dijkstra",
    spots: "hidden",
    color: "GREEN",
    latlng: [52.081451, 5.235404],
  },
  {
    name: "nostalgic bhaskara",
    spots: "inset",
    color: "BLUE",
    latlng: [52.080552, 5.234156],
  },
  {
    name: "silly burnell",
    spots: "ridge",
    color: "BLUE",
    latlng: [52.080598, 5.234361],
  },
  {
    name: "romantic cray",
    spots: "groove",
    color: "GREEN",
    latlng: [52.080253, 5.234631],
  },
  {
    name: "vigilant bose",
    spots: "solid",
    color: "RED",
    latlng: [52.081063, 5.236819],
  },
  {
    name: "quirky buck",
    spots: "dashed",
    color: "GREEN",
    latlng: [52.081163, 5.236446],
  },
  {
    name: "stoic cartwright",
    spots: "double",
    color: "BLUE",
    latlng: [52.080861, 5.235876],
  },
  {
    name: "pensive clarke",
    spots: "dotted",
    color: "YELLOW",
    latlng: [52.081199, 5.234929],
  },
  {
    name: "strange cannon",
    spots: "hidden",
    color: "YELLOW",
    latlng: [52.080273, 5.234664],
  },
  {
    name: "stupefied bohr",
    spots: "none",
    color: "RED",
    latlng: [52.081883, 5.235415],
  },
  {
    name: "pedantic colden",
    spots: "hidden",
    color: "BLUE",
    latlng: [52.081813, 5.236781],
  },
  {
    name: "priceless davinci",
    spots: "dashed",
    color: "GREEN",
    latlng: [52.081538, 5.236694],
  },
  {
    name: "vibrant chandrasekhar",
    spots: "outset",
    color: "RED",
    latlng: [52.081182, 5.237143],
  },
  {
    name: "wizardly booth",
    spots: "groove",
    color: "GREEN",
    latlng: [52.080429, 5.237424],
  },
  {
    name: "suspicious driscoll",
    spots: "inset",
    color: "YELLOW",
    latlng: [52.080074, 5.236462],
  },
  {
    name: "optimistic blackburn",
    spots: "ridge",
    color: "RED",
    latlng: [52.080144, 5.235857],
  },
  {
    name: "reverent curie",
    spots: "groove",
    color: "BLUE",
    latlng: [52.081222, 5.235965],
  },
];

const getMushrooms = (): Promise<MushroomType[]> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(mushrooms);
    }, 1500);
  });
};

export default getMushrooms;

// name: 'objective black',
// name: 'practical curran',
// name: 'recursing darwin',
// name: 'relaxed dhawan',
// name: 'sad chatterjee',
// name: 'serene cohen',
// name: 'sharp dubinsky',
// name: 'sleepy chebyshev',
// name: 'sweet brattain',
// name: 'tender dewdney',
// name: 'thirsty brahmagupta',
// name: 'trusting blackwell',
// name: 'unruffled cerf',
// name: 'upbeat carver',
// name: 'vigorous bouman',
// name: 'wonderful carson',
// name: 'xenodochial borg',
// name: 'youthful brown',
// name: 'zealous boyd',
// name: 'zen chatelet',
