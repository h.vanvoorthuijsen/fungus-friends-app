const fetcher = (input: RequestInfo) => fetch(input).then((res) => res.json());

export default fetcher;
