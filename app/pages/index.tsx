import "leaflet/dist/leaflet.css";

import classNames from "classnames";
import Filters from "components/Filters";
import MushroomMap from "components/MushroomMap";
import fetcher from "helpers/fetcher";
import type { NextPage } from "next";
import Head from "next/head";
import { useState } from "react";
import { MushroomType } from "services/mushrooms/getMushrooms";
import styles from "styles/Home.module.css";
import useSWR from "swr";

import { ApiMushroomsReturnType } from "./api/mushrooms";

const Home: NextPage = () => {
  const { data, error } = useSWR<ApiMushroomsReturnType>(
    "/api/mushrooms",
    fetcher
  );

  const [mushrooms, setMushrooms] = useState<MushroomType[]>([]);

  const loading = !data && !error;

  const handleFilter = (filteredMushrooms: MushroomType[]) => {
    setMushrooms(filteredMushrooms);
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Fungus Friends</title>
        <meta name="description" content="Find all your fungus friends" />
        <link rel="icon" href="/mushrooms.png" />
      </Head>

      <header className={styles.header}>
        {data?.mushRooms && (
          <Filters mushrooms={data.mushRooms} onFilter={handleFilter} />
        )}
      </header>

      <main className={styles.main}>
        <MushroomMap mushrooms={mushrooms} />
        <div
          className={classNames(styles.loadingMessage, {
            [styles.loadingMessageLoading]: loading,
          })}
        >
          Searching for fungi...
        </div>
      </main>
    </div>
  );
};

export default Home;
