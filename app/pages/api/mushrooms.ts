import type { NextApiHandler } from "next";
import getMushrooms, { MushroomType } from "services/mushrooms/getMushrooms";

export type ApiMushroomsReturnType = { mushRooms: MushroomType[] };

const handler: NextApiHandler<ApiMushroomsReturnType> = async (req, res) => {
  if (req.method !== "GET") {
    res.status(405).end();
    return;
  }

  const mushRooms = await getMushrooms();

  res.status(200).json({ mushRooms });
};

export default handler;
