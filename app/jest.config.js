/* eslint-env node */

module.exports = {
  testMatch: ["**/+(*.)+(spec|test).+(ts|tsx|js)"],

  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },

  globals: {
    "ts-jest": {
      compiler: "ttypescript",
    },
  },

  clearMocks: true,

  moduleNameMapper: {
    "\\.scss$": "<rootDir>/__mocks__/styleMock.js",
    "\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/__mocks__/fileMock.js",
  },

  modulePaths: ["<rootDir>"],

  setupFilesAfterEnv: ["./__mocks__/setupTsAutoMock.js"],
};
