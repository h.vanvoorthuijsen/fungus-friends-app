import { latLngBounds } from "leaflet";
import { useEffect } from "react";
import { useMap } from "react-leaflet";
import { MushroomType } from "services/mushrooms/getMushrooms";

type ViewManagerProps = {
  mushrooms: MushroomType[];
};
const ViewManager = ({ mushrooms }: ViewManagerProps): JSXNode => {
  const map = useMap();

  useEffect(() => {
    if (mushrooms.length < 1) return;
    const coordinates = mushrooms.map((mushroom) => mushroom.latlng);
    const bounds = latLngBounds(coordinates);
    map.flyToBounds(bounds, { duration: 2 });
  }, [mushrooms]);
  return null;
};

export default ViewManager;
