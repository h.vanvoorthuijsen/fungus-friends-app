import { MapContainer, TileLayer } from "react-leaflet";
import { MushroomType } from "services/mushrooms/getMushrooms";

import Mushroom from "./Mushroom";
import styles from "./mushroomMap.module.css";
import ViewManager from "./ViewManager";

export type MushroomMapProps = {
  mushrooms: MushroomType[];
};
const MushroomMap = ({ mushrooms }: MushroomMapProps): JSXNode => {
  return (
    <MapContainer
      center={[52.370216, 4.895168]}
      zoom={8}
      scrollWheelZoom={false}
      className={styles.mapContainer}
    >
      <ViewManager mushrooms={mushrooms} />
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {mushrooms.map((mushroom) => (
        <Mushroom mushroom={mushroom} key={mushroom.name} />
      ))}
    </MapContainer>
  );
};

export default MushroomMap;
