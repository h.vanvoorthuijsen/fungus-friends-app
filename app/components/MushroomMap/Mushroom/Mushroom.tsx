import classNames from "classnames";
import { DivIcon } from "leaflet";
import { renderToStaticMarkup } from "react-dom/server";
import { Marker, Popup } from "react-leaflet";
import { MushroomType } from "services/mushrooms/getMushrooms";

import Icon from "./Icon";
import styles from "./mushroom.module.css";

type MushroomProps = {
  mushroom: MushroomType;
};
const Mushroom = ({ mushroom }: MushroomProps): JSXNode => {
  const icon = new DivIcon({
    html: renderToStaticMarkup(<Icon />),
    className: classNames(styles.icon, styles[`color-${mushroom.color}`]),
    iconAnchor: [15, 25],
    popupAnchor: [0, -20],
  });
  return (
    <Marker position={mushroom.latlng} icon={icon}>
      <Popup>
        <h2 className={styles.name}>{mushroom.name}</h2>
        <ul className={styles.properties}>
          <li className={styles.property}>
            <span className={styles.propertyName}>Spots:</span>{" "}
            <span
              className={styles.propertySpots}
              style={{ borderBottomStyle: mushroom.spots }}
            >
              {mushroom.spots}
            </span>
          </li>
          <li className={styles.property}>
            <span className={styles.propertyName}>Color:</span>{" "}
            <span className={styles[`color-${mushroom.color}`]}>
              {mushroom.color.toLowerCase()}
            </span>
          </li>
        </ul>
      </Popup>
    </Marker>
  );
};

export default Mushroom;
