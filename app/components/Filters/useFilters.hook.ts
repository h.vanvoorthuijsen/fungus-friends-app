import { useEffect, useState } from "react";
import { SingleValue } from "react-select";
import {
  ColorType,
  MushroomType,
  SpotsType,
} from "services/mushrooms/getMushrooms";

import {
  isMushroomWithColor,
  isMushroomWithSpots,
} from "./helpers/filterMushrooms";
import { getColorOptions, getSpotsOptions } from "./helpers/getOptions";

const useFilters = (
  mushrooms: MushroomType[],
  onFilter: (mushrooms: MushroomType[]) => void
) => {
  const [selectedColorOption, setSelectedColorOption] =
    useState<OptionType<ColorType>>();
  const [selectedSpotsOption, setSelectedSpotsOption] =
    useState<OptionType<SpotsType>>();

  useEffect(() => {
    onFilter(
      mushrooms
        .filter(isMushroomWithColor(selectedColorOption?.value))
        .filter(isMushroomWithSpots(selectedSpotsOption?.value))
    );
  }, [selectedColorOption, selectedSpotsOption, mushrooms]);

  const handleColorChange = (
    option: SingleValue<OptionType<ColorType>> | null
  ) => {
    if (!option) {
      setSelectedColorOption(undefined);
      return false;
    }
    setSelectedColorOption(option);
  };

  const handleSpotsChange = (
    option: SingleValue<OptionType<SpotsType>> | null
  ) => {
    if (!option) {
      setSelectedSpotsOption(undefined);
      return false;
    }
    setSelectedSpotsOption(option);
  };

  const availableColors = getColorOptions(
    mushrooms,
    selectedSpotsOption?.value
  );

  const availableSpots = getSpotsOptions(mushrooms, selectedColorOption?.value);

  return {
    availableColors,
    availableSpots,
    handleColorChange,
    handleSpotsChange,
    selectedColorOption,
    selectedSpotsOption,
  };
};

export default useFilters;

type OptionType<T> = { value: T; label: string };
