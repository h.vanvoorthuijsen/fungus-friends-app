import {
  ColorType,
  MushroomType,
  SpotsType,
} from "services/mushrooms/getMushrooms";

export const isMushroomWithColor =
  (color?: ColorType) => (mushroom: MushroomType) =>
    color ? mushroom.color === color : true;

export const isMushroomWithSpots =
  (spots?: SpotsType) => (mushroom: MushroomType) =>
    spots ? mushroom.spots === spots : true;
