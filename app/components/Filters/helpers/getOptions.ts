import {
  ColorType,
  MushroomType,
  SpotsType,
} from "services/mushrooms/getMushrooms";

import { isMushroomWithColor, isMushroomWithSpots } from "./filterMushrooms";

export const mapValueToOption = <T extends string>(value: T) => ({
  value,
  label: value.toLowerCase(),
});

export const getColorOptions = (
  mushrooms: MushroomType[],
  selectedSpots?: SpotsType
) =>
  [
    ...new Set(
      mushrooms
        .filter(isMushroomWithSpots(selectedSpots))
        .map(({ color }) => color)
    ),
  ].map(mapValueToOption);

export const getSpotsOptions = (
  mushrooms: MushroomType[],
  selectedColor?: ColorType
) =>
  [
    ...new Set(
      mushrooms
        .filter(isMushroomWithColor(selectedColor))
        .map(({ spots }) => spots)
    ),
  ].map(mapValueToOption);
