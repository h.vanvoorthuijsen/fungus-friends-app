import { MushroomType } from "services/mushrooms/getMushrooms";
import { createMock } from "ts-auto-mock";

import { isMushroomWithColor, isMushroomWithSpots } from "./filterMushrooms";

describe("Filter helpers", () => {
  describe("isMushroomWithColor", () => {
    const mushroom = createMock<MushroomType>({ color: "BLUE" });

    describe("When the mushroom matches the color", () => {
      const isBlueMushroom = isMushroomWithColor("BLUE")(mushroom);
      test("Then it will return true", () => {
        expect(isBlueMushroom).toBeTruthy();
      });
    });

    describe("When the mushroom does not match the color", () => {
      const isGreenMushroom = isMushroomWithColor("GREEN")(mushroom);
      test("Then it will return false", () => {
        expect(isGreenMushroom).toBeFalsy();
      });
    });

    describe("When the color is not set", () => {
      const result = isMushroomWithColor()(mushroom);
      test("Then it will return always true", () => {
        expect(result).toBeTruthy();
      });
    });
  });

  describe("isMushroomWithSpots", () => {
    const mushroom = createMock<MushroomType>({ spots: "groove" });

    describe("When the mushroom matches the spots", () => {
      const isGroovyMushroom = isMushroomWithSpots("groove")(mushroom);
      test("Then it will return true", () => {
        expect(isGroovyMushroom).toBeTruthy();
      });
    });

    describe("When the mushroom does not match the spots", () => {
      const isDottedMushroom = isMushroomWithSpots("dotted")(mushroom);
      test("Then it will return false", () => {
        expect(isDottedMushroom).toBeFalsy();
      });
    });

    describe("When the spots is not set", () => {
      const result = isMushroomWithSpots()(mushroom);
      test("Then it will return always true", () => {
        expect(result).toBeTruthy();
      });
    });
  });
});
