import { MushroomType } from "services/mushrooms/getMushrooms";
import { createMock } from "ts-auto-mock";

import {
  getColorOptions,
  getSpotsOptions,
  mapValueToOption,
} from "./getOptions";

describe("get options helpers", () => {
  describe("mapValueToOption", () => {
    describe("When the value is `Abc`", () => {
      const option = mapValueToOption("Abc");

      test("Then the option has the correct properties", () => {
        expect(option).toHaveProperty("value", "Abc");
        expect(option).toHaveProperty("label", "abc");
      });

      test("And the label is lowercased", () => {
        expect(option.label).toEqual("abc");
      });
    });
  });

  const mushrooms = [
    createMock<MushroomType>({ color: "BLUE", spots: "dashed" }),
    createMock<MushroomType>({ color: "GREEN", spots: "dotted" }),
    createMock<MushroomType>({ color: "YELLOW", spots: "dashed" }),
    createMock<MushroomType>({ color: "BLUE", spots: "groove" }),
  ];

  describe("getColorOptions", () => {
    describe("When the spots option is not set", () => {
      const result = getColorOptions(mushrooms, undefined);
      const colors = result.map(({ value }) => value);
      test("Then all colors are in the option list", () => {
        expect(colors).toEqual(["BLUE", "GREEN", "YELLOW"]);
        expect(result).toHaveLength(3);
      });
    });

    describe("When the spots options is set", () => {
      const result = getColorOptions(mushrooms, "dashed");
      const colors = result.map(({ value }) => value);
      test("Then only the colors that match the spots are in the list", () => {
        expect(colors).toEqual(["BLUE", "YELLOW"]);
        expect(result).toHaveLength(2);
      });
    });
  });

  describe("getSpotsOptions", () => {
    describe("When the color option is not set", () => {
      const result = getSpotsOptions(mushrooms, undefined);
      const spots = result.map(({ value }) => value);
      test("Then all spots are in the option list", () => {
        expect(spots).toEqual(["dashed", "dotted", "groove"]);
        expect(result).toHaveLength(3);
      });
    });

    describe("When the color options is set", () => {
      const result = getSpotsOptions(mushrooms, "BLUE");
      const spots = result.map(({ value }) => value);
      test("Then only the spots that match the color are in the list", () => {
        expect(spots).toEqual(["dashed", "groove"]);
        expect(result).toHaveLength(2);
      });
    });
  });
});
