import Select from "react-select";
import { MushroomType } from "services/mushrooms/getMushrooms";

import styles from "./filters.module.css";
import useFilters from "./useFilters.hook";

type FiltersProps = {
  mushrooms: MushroomType[];
  onFilter: (mushrooms: MushroomType[]) => void;
};

const Filters = ({ mushrooms, onFilter }: FiltersProps): JSXNode => {
  const {
    availableColors,
    availableSpots,
    handleColorChange,
    handleSpotsChange,
    selectedColorOption,
    selectedSpotsOption,
  } = useFilters(mushrooms, onFilter);

  return (
    <div className={styles.filterContainer}>
      <Select
        options={availableColors}
        onChange={handleColorChange}
        value={selectedColorOption}
        isClearable
        placeholder="Select a color..."
        className={styles.filter}
      />
      <Select
        options={availableSpots}
        onChange={handleSpotsChange}
        value={selectedSpotsOption}
        isClearable
        placeholder="Select type of spots..."
        className={styles.filter}
      />
    </div>
  );
};

export default Filters;
