# Fungus Friends App

All kinds of fungus friends can be found in the garden of SpronQ. With this app you can find where!

## What do you need to get it running

- GNU Make
- Docker with Docker-compose

## How to get it running for the first time

Just run `make init`

## Further commands

Run `make help` to get a list of all available commands
