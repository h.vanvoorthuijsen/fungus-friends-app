help:
	@cat $(MAKEFILE_LIST) | docker run --rm -i xanders/make-help

#
# Snippets
#

docker-compose = docker-compose
docker-compose-run = docker-compose run --rm
docker-compose-exec = docker-compose exec
docker-compose-up = docker-compose up -d --remove-orphans

##
## Generic
##

# Initialize the project
init: \
	do-intro \
	do-app-updates \
	do-start

# Start the project
start: \
	do-intro \
	do-start

# Stop the project
stop: \
	do-intro \
	do-stop

# Check the code for linting errors
lint: \
	do-intro \
	do-app-lint

# Fix all auto-fixable linting errors
fix: \
	do-intro \
	do-app-lint-fix

# Update all components
update: \
	do-intro \
	do-app-updates

do-intro:
	@echo ""
	@echo "  .-\"\"\"-."
	@echo " /* * * *\\"
	@echo ":_.-:\`:-._;"
	@echo "    (_)"
	@echo " \|/(_)\\|\\"
	@echo ""

do-start:
	@echo "=== Starting project ===\n"
	@${docker-compose-up}
	@echo "> The project is running on http://localhost:3000"

do-stop:
	@echo "=== Stopping project ===\n"
	@${docker-compose} stop

##
## App
##

# Open shell in app container
app-shell: \
	do-intro \
	do-app-shell

do-app-shell:
	@echo "=== Open app shell ===\n"
	@${docker-compose-run} app sh

do-app-lint:
	@echo "=== Linting app ===\n"
	@${docker-compose-run} app npm run prettier-eslint:lint

do-app-lint:
	@echo "=== Linting app ===\n"
	@${docker-compose-run} app npm run eslint:lint && echo "> All good 👍"
	@${docker-compose-run} app npm run prettier-eslint:lint && echo "> All good 👍"

do-app-lint-fix:
	@echo "=== Fixing linting errors in app ===\n"
	@${docker-compose-run} app npm run prettier-eslint:fix && echo "> All good 👍"

do-app-updates:
	@echo "=== Updating app ===\n"
	@${docker-compose-run} app yarn install
